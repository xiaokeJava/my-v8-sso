package interfaces.sso;

import base.service.IBaseService;
import v8.entity.TUser;

import java.util.ArrayList;

public interface ISSOService extends IBaseService<TUser> {


    boolean checkLogin(TUser user);

    ArrayList<String> SecKillService(String i, ArrayList<String> results);

    void toCheck(ArrayList<String> objects);
}
