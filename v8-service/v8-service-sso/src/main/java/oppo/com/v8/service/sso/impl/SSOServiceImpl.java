package oppo.com.v8.service.sso.impl;

import base.dao.IBaseDao;
import base.service.impl.BaseServiceImpl;
import com.alibaba.dubbo.config.annotation.Service;
import interfaces.sso.ISSOService;
import org.springframework.beans.factory.annotation.Autowired;
import v8.entity.TStudent;
import v8.entity.TUser;
import v8.mapper.TStudentMapper;
import v8.mapper.TUserMapper;

import java.util.ArrayList;

@Service
public class SSOServiceImpl extends BaseServiceImpl<TUser> implements ISSOService {
   @Autowired
    private TUserMapper userMapper;
   @Autowired
    private TStudentMapper studentMapper;
    @Override
    public IBaseDao<TUser> getBaseDao() {
        return userMapper;
    }

    @Override
    public boolean checkLogin(TUser user) {
        TUser tuser = userMapper.selectByPrimaryKey((long) 1);
        TStudent student=studentMapper.checkLogin(user.getUsername());
        if (student!=null){
            if (student.getPassword().equals(user.getPassword())){
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized ArrayList<String> SecKillService(String i, ArrayList<String> results) {
        results.add(i);
            /*if (Integer.parseInt(i)<5){
                return true;
            }*/
        return results;
    }

    @Override
    public void toCheck(ArrayList<String> objects) {
        objects.add("xiaoke");
    }
}
