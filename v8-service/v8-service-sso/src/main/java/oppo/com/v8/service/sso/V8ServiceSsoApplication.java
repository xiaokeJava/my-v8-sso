package oppo.com.v8.service.sso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath:provider.xml")
@MapperScan("v8.mapper")
public class V8ServiceSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(V8ServiceSsoApplication.class, args);
    }
}
