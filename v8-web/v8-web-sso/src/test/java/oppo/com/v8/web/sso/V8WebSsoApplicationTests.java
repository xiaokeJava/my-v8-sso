package oppo.com.v8.web.sso;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class V8WebSsoApplicationTests {

    @Test
    public void contextLoads() {
    }

}
