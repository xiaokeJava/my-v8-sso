package oppo.com.v8.web.sso.controller;

import interfaces.sso.ISSOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import v8.entity.TUser;

import java.util.ArrayList;
import java.util.concurrent.*;

@Controller
@RequestMapping("ssoController")
public class SSOController {
    //获取电脑cpu核数
    private static int corePoolSize=Runtime.getRuntime().availableProcessors();
    //创建线程池
    private static ExecutorService executorService=new ThreadPoolExecutor(
    corePoolSize,corePoolSize,0L, TimeUnit.MICROSECONDS,new LinkedBlockingQueue<>());
    @Autowired
    private ISSOService ssoService;
    @RequestMapping("goLogin")
    public String goLogin(){
        return "login";
    }
    @RequestMapping("checkLogin")
    public String checkLogin(TUser user, Model model){
        boolean result=ssoService.checkLogin(user);
        if (result==true){
            return "index";
        }
        System.out.println("网上更新");
        model.addAttribute("message","密码错误");
        return "login";
    }
    @ResponseBody
    @RequestMapping("toCheck")
    public ArrayList<String> toCheck(){
        ArrayList<String> objects = new ArrayList<>();
        ssoService.toCheck(objects);
        for (String object : objects) {
            System.out.println(object);
        }
        return objects;
    }
    @ResponseBody
    @RequestMapping("toSecKill")
    public String toSecKill(){
        ArrayList<String> results = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            results = miaosha(i+"",results);
        }
        /*int j=0;
        for (Boolean object : objects) {
            if (object){
                j++;
            }
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(results.get(i));
        }*/
        for (int i = 0; i < 5; i++) {
            System.out.println(results.get(i));
        }
        return 1+"";
    }

    public ArrayList<String> miaosha(String i,ArrayList<String> results){
        Future<ArrayList<String>> result = executorService.submit(new CreatePage(i,results));
        ArrayList<String> flag =null;
        try {
             flag=result.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public ArrayList<String> SecKill(String i,ArrayList<String> results){
        return ssoService.SecKillService(i,results);
    }
    class CreatePage implements Callable<ArrayList<String>> {
        private String i;
        private ArrayList<String> results;
        public CreatePage(String i,ArrayList<String> results) {
            this.i=i;
            this.results=results;
        }
        @Override public ArrayList<String> call() throws Exception {
            return SecKill(i,results);
        }
    }
}
