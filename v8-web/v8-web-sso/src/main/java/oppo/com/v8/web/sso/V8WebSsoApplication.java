package oppo.com.v8.web.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;

//@SpringBootApplication
@ImportResource("classpath:consumer.xml")
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class V8WebSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(V8WebSsoApplication.class, args);
    }
}

