package v8.mapper;

import base.dao.IBaseDao;
import v8.entity.TUser;

public interface TUserMapper extends IBaseDao<TUser> {

}
