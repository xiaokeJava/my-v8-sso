package v8.mapper;

import base.dao.IBaseDao;
import v8.entity.TStudent;

public interface TStudentMapper extends IBaseDao {

    TStudent checkLogin(String username);
}