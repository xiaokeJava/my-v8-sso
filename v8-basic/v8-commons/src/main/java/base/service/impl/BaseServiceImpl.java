package base.service.impl;

import base.dao.IBaseDao;
import base.service.IBaseService;

public abstract class BaseServiceImpl<T> implements IBaseService<T> {
    public abstract IBaseDao<T> getBaseDao();
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    public int insert(T t) {
        return 0;
    }

    public int insertSelective(T t) {
        return 0;
    }

    public T selectByPrimaryKey(Long id) {
        return null;
    }

    public int updateByPrimaryKeySelective(T t) {
        return 0;
    }

    public int updateByPrimaryKey(T t) {
        return 0;
    }
}
